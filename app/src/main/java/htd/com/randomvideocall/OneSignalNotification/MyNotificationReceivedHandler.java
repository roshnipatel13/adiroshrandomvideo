package htd.com.randomvideocall.OneSignalNotification;

import android.content.SharedPreferences;
import android.util.Log;

import com.onesignal.OSNotification;
import com.onesignal.OneSignal;

import androidx.appcompat.app.AppCompatActivity;
import htd.com.randomvideocall.CentralFbAds;
import htd.com.randomvideocall.DbHelper.OneSignalDBHelper;

public class MyNotificationReceivedHandler extends AppCompatActivity implements OneSignal.NotificationReceivedHandler {
    public static int counter;
    public OneSignalDBHelper myDb = new OneSignalDBHelper(CentralFbAds.getContext());

    @Override
    public void notificationReceived(OSNotification notification) {
        String notification_Img = notification.payload.bigPicture;
        String notification_launchUrl = notification.payload.launchURL;
        String notification_Text = notification.payload.title;
        int notificationId = notification.androidNotificationId;
        myDb.insertNotification(notificationId, notification_Text, notification_Img, notification_launchUrl);
    }
}