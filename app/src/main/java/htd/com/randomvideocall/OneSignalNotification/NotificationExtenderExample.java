package htd.com.randomvideocall.OneSignalNotification;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.onesignal.NotificationExtenderService;
import com.onesignal.OSNotificationDisplayedResult;
import com.onesignal.OSNotificationReceivedResult;

import java.math.BigInteger;

import androidx.core.app.NotificationCompat;
import htd.com.randomvideocall.CentralFbAds;
import htd.com.randomvideocall.R;

import static htd.com.randomvideocall.OneSignalNotification.MyNotificationReceivedHandler.counter;

public class NotificationExtenderExample extends NotificationExtenderService {

    @Override
    protected boolean onNotificationProcessing(OSNotificationReceivedResult receivedResult) {
        OverrideSettings overrideSettings = new OverrideSettings();
        overrideSettings.extender = new NotificationCompat.Extender() {
            @Override
            public NotificationCompat.Builder extend(NotificationCompat.Builder builder) {
                // Sets the background notification color to Red on Android 5.0+ devices.
                Bitmap icon = BitmapFactory.decodeResource(CentralFbAds.getContext().getResources(),
                        R.drawable.room3);
                builder.setLargeIcon(icon);
                return builder.setColor(new BigInteger("FF0000FF", 16).intValue());
            }
        };
        SharedPreferences prefs1 = getSharedPreferences("RVdoNotificationCounter", MODE_PRIVATE);
        counter = prefs1.getInt("notificationCounterRVdo", counter);

        counter++;
        SharedPreferences.Editor editor1 = getSharedPreferences("RVdoNotificationCounter", MODE_PRIVATE).edit();
        editor1.putInt("notificationCounterRVdo", counter);
        editor1.apply();

        OSNotificationDisplayedResult displayedResult = displayNotification(overrideSettings);
        Log.d("OneSignalExample", "Notification displayed with id: " + displayedResult.androidNotificationId);

        return true;
    }
}
