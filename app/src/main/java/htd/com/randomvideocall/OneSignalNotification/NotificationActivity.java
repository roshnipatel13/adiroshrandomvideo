package htd.com.randomvideocall.OneSignalNotification;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;
import htd.com.randomvideocall.CentralFbAds;
import htd.com.randomvideocall.DbHelper.OneSignalDBHelper;
import htd.com.randomvideocall.R;
import htd.com.randomvideocall.adapter.NotificationAdapter;
import htd.com.randomvideocall.model.NotificationModel;

public class NotificationActivity extends AppCompatActivity {
    private Activity mActivity;
    private Context mContext;
    public ArrayList<NotificationModel> list = new ArrayList<NotificationModel>();
    public RecyclerView notificationRecycler;
    public ItemTouchHelper itemTouchHelper;
    NotificationAdapter notificationAdapter;
    OneSignalDBHelper mydb = new OneSignalDBHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        initVars();

        OneSignalDBHelper mydb = new OneSignalDBHelper(this);

        list = mydb.getAllNotificationData();
        Collections.reverse(list);

        LinearLayout adviewBanner = findViewById(R.id.fbbanner);
        if (CentralFbAds.isNetworkConnected(this)) {
            adviewBanner.addView(CentralFbAds.SetupBannerAds(0));
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Notifications");
        }
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setTitle("Notifications");
        toolbar.setElevation(0);
        toolbar.setNavigationIcon(R.drawable.back_top_bar_icon);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyNotificationReceivedHandler.counter = 0;
                finish();
            }
        });
        notificationRecycler = findViewById(R.id.notificationRecycler);
        notificationRecycler.setLayoutManager(new GridLayoutManager(this, 1));
        notificationRecycler.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        notificationAdapter = new NotificationAdapter(this, list);
        notificationRecycler.setAdapter(notificationAdapter);

        ItemTouchHelper.Callback callback = new ItemTouchHelperClass(notificationAdapter);
        itemTouchHelper = new ItemTouchHelper(callback);
        itemTouchHelper.attachToRecyclerView(notificationRecycler);
    }

    private void initVars() {
        mActivity = NotificationActivity.this;
        mContext = mActivity.getApplicationContext();
    }

    public void refreshDB() {
        list.clear();
        list.addAll(mydb.getAllNotificationData());
        Collections.reverse(list);
        notificationRecycler.invalidateItemDecorations();
        notificationRecycler.refreshDrawableState();
        notificationAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.notification_refresh_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int Id = item.getItemId();
        if (Id == R.id.action_refresh) {
            refreshDB();
            Toast.makeText(getApplicationContext(), "Notifications are Refreshed", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        MyNotificationReceivedHandler.counter = 0;
        super.onBackPressed();
    }
}