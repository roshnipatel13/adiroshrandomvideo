package htd.com.randomvideocall.OneSignalNotification;

import android.content.Intent;

import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import androidx.appcompat.app.AppCompatActivity;
import htd.com.randomvideocall.CentralFbAds;
import htd.com.randomvideocall.DbHelper.OneSignalDBHelper;

public class MyNotificationOpenedHandler extends AppCompatActivity implements OneSignal.NotificationOpenedHandler {
    OneSignalDBHelper mydb = new OneSignalDBHelper(CentralFbAds.getContext());

    @Override
    public void notificationOpened(OSNotificationOpenResult result) {

        int Nid = result.notification.androidNotificationId;
        mydb.deleteNotification(Nid);

        MyNotificationReceivedHandler.counter = MyNotificationReceivedHandler.counter - 1;

        if (result.notification.payload.launchURL == null) {
            try {
                Intent i = CentralFbAds.getContext().getPackageManager().getLaunchIntentForPackage("htd.com.randomvideocall");
                CentralFbAds.getContext().startActivity(i);
            } catch (Exception e) {
                // TODO Auto-generated catch block
            }
        }
    }
}
