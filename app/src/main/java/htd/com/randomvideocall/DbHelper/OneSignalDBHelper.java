package htd.com.randomvideocall.DbHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

import htd.com.randomvideocall.model.NotificationModel;

public class OneSignalDBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "RandomVdoNotification.db";
    public static final String TABLE_NAME = "notificationData";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_TEXT = "NotificationText";
    public static final String COLUMN_IMGPATH = "NotificationImgPath";
    public static final String COLUMN_URL = "NotificationURL";
    public static final String COLUMN_NID = "NotificationId";

    private HashMap hp;

    public OneSignalDBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.e("OneSignal","CallingDBHelper");

        // TODO Auto-generated method stub
        db.execSQL(
                "create table notificationData " +
                        "(id integer primary key,NotificationId integer, NotificationText text,NotificationImgPath text,NotificationURL text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS notificationData");
        onCreate(db);
    }

    public boolean insertNotification(int NotificationId, String NotificationText, String NotificationImgPath, String NotificationURL) {
        SQLiteDatabase db = this.getWritableDatabase();
        Log.e("Db","InsertingData");
        ContentValues contentValues = new ContentValues();
        contentValues.put("NotificationId", NotificationId);
        contentValues.put("NotificationText", NotificationText);
        contentValues.put("NotificationImgPath", NotificationImgPath);
        contentValues.put("NotificationURL", NotificationURL);

        db.insert("notificationData", null, contentValues);
        return true;
    }

    public Cursor getData(int Nid) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from notificationData where NotidicationId=" + Nid + "", null);
        return res;
    }

    public String getNIDData(int pos) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select NotidicationId from notificationData", null);
        return res.getString(pos);
    }

    public int numberOfRows() {
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, TABLE_NAME);
        return numRows;
    }

    public Integer deleteNotification(Integer Nid) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("notificationData",
                "NotificationId = ? ",
                new String[]{Integer.toString(Nid)});
    }

    public ArrayList<NotificationModel> getAllNotificationData() {
        ArrayList<NotificationModel> noti_data = new ArrayList<NotificationModel>();


        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from notificationData", null);
        res.moveToFirst();

        while (res.isAfterLast() == false) {

            NotificationModel notificationModel = new NotificationModel();
            notificationModel.setNotification_Text((res.getString(res.getColumnIndex(COLUMN_TEXT))));
            notificationModel.setNotification_launchUrl((res.getString(res.getColumnIndex(COLUMN_URL))));
            notificationModel.setNotification_Img((res.getString(res.getColumnIndex(COLUMN_IMGPATH))));
            notificationModel.setNotification_Id((res.getInt(res.getColumnIndex(COLUMN_NID))));


            res.moveToNext();

            noti_data.add(notificationModel);
        }

        return noti_data;
    }
}