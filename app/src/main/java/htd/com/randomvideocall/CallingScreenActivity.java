package htd.com.randomvideocall;

import android.content.Context;
import android.hardware.Camera;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.HttpDataSource;

import java.io.IOException;
import java.util.concurrent.ThreadLocalRandom;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageButton;
import htd.com.randomvideocall.Utils.CameraPreview;

public class CallingScreenActivity extends AppCompatActivity implements SurfaceHolder.Callback, Camera.ShutterCallback, Camera.PictureCallback, Player.EventListener {

    PlayerView simpleExoPlayerView;
    SimpleExoPlayer exoPlayer;
    String videourl = "https://videostatus.sgp1.cdn.digitaloceanspaces.com/randomvideocall/";
    boolean volume = true;
    ProgressBar loading;
    int randomVideo = ThreadLocalRandom.current().nextInt(1, 120 + 1);
    private String status;
    private SurfaceView mPreview;
    private FrameLayout cameraPreview;
    private Camera mCamera;
    private Context myContext;
    private boolean cameraFront = false;
    private AppCompatImageButton switchCamera, btnSpeaker, btnMicrophone, btnCallEnd;
    private AudioManager audioManager;
    private int currentCameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert cm != null;
        return cm.getActiveNetworkInfo() != null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calling_screen);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        myContext = this;

        simpleExoPlayerView = findViewById(R.id.player_view);
        try {
            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            TrackSelector trackSelector = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(bandwidthMeter));
            exoPlayer = ExoPlayerFactory.newSimpleInstance(this, trackSelector);
            Uri videoUri = Uri.parse(videourl + randomVideo + ".mp4");
            DefaultHttpDataSourceFactory dataSourceFactory = new DefaultHttpDataSourceFactory("player");
            ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
            MediaSource mediaSource = new ExtractorMediaSource(videoUri, dataSourceFactory, extractorsFactory, null,
                    null);
            simpleExoPlayerView.setPlayer(exoPlayer);
            exoPlayer.prepare(mediaSource);
            exoPlayer.setPlayWhenReady(true);
            exoPlayer.addListener(this);

        } catch (Exception e) {
            Log.e("error:", "video can not be loaded");
        }

        loading = findViewById(R.id.loading);

        btnSpeaker = findViewById(R.id.btnSpeaker);
        btnSpeaker.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (volume) {
                    btnSpeaker.setImageResource(R.drawable.ic_speaker_off);
                    exoPlayer.setVolume(0);
                    volume = false;
                } else {
                    btnSpeaker.setImageResource(R.drawable.ic_speaker_on);
                    exoPlayer.setVolume(10);
                    volume = true;
                }
            }
        });

        btnMicrophone = findViewById(R.id.btnMicrophone);
        audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        btnMicrophone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                audioManager.setMode(AudioManager.MODE_IN_CALL);
                if (!audioManager.isMicrophoneMute()) {
                    btnMicrophone.setImageResource(R.drawable.ic_mic_off);
                    audioManager.setMicrophoneMute(true);
                } else {
                    audioManager.setMicrophoneMute(false);
                    btnMicrophone.setImageResource(R.drawable.ic_mic_on);
                }
            }
        });

        btnCallEnd = findViewById(R.id.btnCallEnd);
        btnCallEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mCamera = Camera.open(currentCameraId);
        mCamera.setDisplayOrientation(90);
        cameraPreview = findViewById(R.id.frontCamera);
        mPreview = new CameraPreview(myContext, mCamera);
        cameraPreview.addView(mPreview);

        switchCamera = findViewById(R.id.btnCamera);
        switchCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCamera.stopPreview();
//NB: if you don't release the current camera before switching, you app will crash
                mCamera.release();
//swap the id of the camera to be used
                if (currentCameraId == Camera.CameraInfo.CAMERA_FACING_BACK) {
                    currentCameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
                } else {
                    currentCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
                }
                mCamera = Camera.open(currentCameraId);
                mCamera.setDisplayOrientation(90);
                try {
                    mCamera.setPreviewDisplay(mPreview.getHolder());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                mCamera.startPreview();
            }
        });
        mCamera.startPreview();
    }

    @Override
    public void onResume() {
        resumePlayer();
        super.onResume();
        if (mCamera == null) {
            mCamera = Camera.open(currentCameraId);
            mCamera.setDisplayOrientation(90);
            //  mPreview.refreshCamera(mCamera);
            Log.d("nu", "null");
        } else {
            Log.d("nu", "no null");
        }

    }

    private void releasePlayer() {
        if (exoPlayer != null) {
            exoPlayer.release();
            exoPlayer = null;
        }
    }

    private void pausePlayer() {
        if (exoPlayer != null) {
            exoPlayer.setPlayWhenReady(false);
            exoPlayer.getPlaybackState();
        }
    }

    private void resumePlayer() {
        if (exoPlayer != null) {
            exoPlayer.setPlayWhenReady(true);
            exoPlayer.getPlaybackState();
        }
    }

    @Override
    protected void onDestroy() {
        releasePlayer();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        pausePlayer();
        //when on Pause, release camera in order to be used from other applications
        //releaseCamera();
    }

    private boolean isPlaying() {
        return exoPlayer != null
                && exoPlayer.getPlaybackState() != Player.STATE_ENDED
                && exoPlayer.getPlaybackState() != Player.STATE_IDLE
                && exoPlayer.getPlayWhenReady();
    }

    private void releaseCamera() {
        // stop and release camera
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.setPreviewCallback(null);
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {

    }

    @Override
    public void onShutter() {

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            mCamera.setPreviewDisplay(mPreview.getHolder());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(Camera.CameraInfo.CAMERA_FACING_FRONT, info);
        int rotation = this.getWindowManager().getDefaultDisplay()
                .getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break; // Natural orientation
            case Surface.ROTATION_90:
                degrees = 90;
                break; // Landscape left
            case Surface.ROTATION_180:
                degrees = 180;
                break;// Upside down
            case Surface.ROTATION_270:
                degrees = 270;
                break;// Landscape right
        }
        int rotate = (info.orientation - degrees + 360) % 360;
        Camera.Parameters params = mCamera.getParameters();
        params.setRotation(rotate);
        mCamera.setParameters(params);
        mCamera.setDisplayOrientation(90);
        mCamera.startPreview();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {
    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        switch (playbackState) {
            case Player.STATE_BUFFERING:
                if (!isNetworkConnected(this)) {
                    Toast.makeText(getApplicationContext(),"Please Check Your Internet Connection..",Toast.LENGTH_SHORT).show();
                    loading.setVisibility(View.VISIBLE);
                }
                loading.setVisibility(View.VISIBLE);
                break;
            case Player.STATE_ENDED:
                finish();
                break;
            case Player.STATE_IDLE:
                if (!isNetworkConnected(this)) {
                    Toast.makeText(getApplicationContext(),"Please Check Your Internet Connection..",Toast.LENGTH_SHORT).show();
                    loading.setVisibility(View.VISIBLE);
                }
                loading.setVisibility(View.GONE);
                break;
            case Player.STATE_READY:
                if (!isNetworkConnected(this)) {
                    Toast.makeText(getApplicationContext(),"Please Check Your Internet Connection..",Toast.LENGTH_SHORT).show();
                    loading.setVisibility(View.VISIBLE);
                }
                loading.setVisibility(View.GONE);
                break;
            default:
                if (!isNetworkConnected(this)) {
                    loading.setVisibility(View.VISIBLE);
                    Toast.makeText(getApplicationContext(),"Please Check Your Internet Connection..",Toast.LENGTH_SHORT).show();
                }
                loading.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {
    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {
        if (!isNetworkConnected(this)) {
            Toast.makeText(getApplicationContext(), "Please Check Your Internet Connection", Toast.LENGTH_SHORT).show();
            finish();
        }
        if (error.type == ExoPlaybackException.TYPE_SOURCE) {
            IOException cause = error.getSourceException();
            if (cause instanceof HttpDataSource.HttpDataSourceException) {
                // An HTTP error occurred.
                HttpDataSource.HttpDataSourceException httpError = (HttpDataSource.HttpDataSourceException) cause;
                // This is the request for which the error occurred.
                DataSpec requestDataSpec = httpError.dataSpec;
                // It's possible to find out more about the error both by casting and by
                // querying the cause.
                if (httpError instanceof HttpDataSource.InvalidResponseCodeException) {
                    // Cast to InvalidResponseCodeException and retrieve the response code,
                    // message and headers.
                } else {
                    // Try calling httpError.getCause() to retrieve the underlying cause,
                    // although note that it may be null.
                }
            }
        }
    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {
    }
}