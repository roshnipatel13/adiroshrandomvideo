package htd.com.randomvideocall;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdIconView;
import com.facebook.ads.AdOptionsView;
import com.facebook.ads.AudienceNetworkAds;
import com.facebook.ads.MediaView;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdLayout;
import com.facebook.ads.NativeAdListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ThreadLocalRandom;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class ConnectActivity extends AppCompatActivity {
    private final String TAG = ConnectActivity.class.getSimpleName();
    TextView randomCount, time;
    CountDownTimer c;
    Timer timeoutTimer;
    GenerateTask genTask = new GenerateTask();
    int permsRequestCode = 200;
    Activity activity = ConnectActivity.this;
    private NativeAd nativeAd;
    private NativeAdLayout nativeAdLayout;

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert cm != null;
        return cm.getActiveNetworkInfo() != null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect);

        AudienceNetworkAds.initialize(this);
        loadNativeAd();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED)
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, permsRequestCode);

        if (!genTask.started) {
            genTask.started = true;
            timeoutTimer = new Timer();
            timeoutTimer.scheduleAtFixedRate(genTask, 0, 1000);
        } else {
            genTask.started = false;
            timeoutTimer.cancel();
        }

        LinearLayout adviewBanner = findViewById(R.id.fbbanner);
        if (CentralFbAds.isNetworkConnected(this)) {
            adviewBanner.addView(CentralFbAds.SetupBannerAds(0));
        }

        time = findViewById(R.id.time);
        time.setText("30");

        c = new CountDownTimer(30000, 1000) {
            int test = ThreadLocalRandom.current().nextInt(10, 25 + 1);

            public void onTick(long millisUntilFinished) {
                time.setText("" + millisUntilFinished / 1000);
                if (time.getText().toString().equals(test + "")) {
                    c.cancel();
                    c.onFinish();
                    if (!isNetworkConnected(ConnectActivity.this)) {
                        startActivity(new Intent(ConnectActivity.this,MainActivity.class));
                        Toast.makeText(getApplicationContext(), "Please Check Your Internet Connection..", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            public void onFinish() {
                Intent i = new Intent(ConnectActivity.this, CallingScreenActivity.class);
                startActivity(i);
                finish();
            }
        };
        c.start();
    }

    @Override
    public void onBackPressed() {
        c.cancel();
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        c.cancel();
        super.onPause();
    }

    @Override
    protected void onResume() {
        c.start();
        super.onResume();
    }

    private void loadNativeAd() {
        // Instantiate a NativeAd object.
        // NOTE: the placement ID will eventually identify this as your App, you can ignore it for
        // now, while you are testing and replace it later when you have signed up.
        // While you are using this temporary code you will only get test ads and if you release
        // your code like this to the Google Play your users will not receive ads (you will get a no fill error).
        nativeAd = new NativeAd(this, "2559840224280840_2560564427541753");

        nativeAd.setAdListener(new NativeAdListener() {
            @Override
            public void onMediaDownloaded(Ad ad) {
                // Native ad finished downloading all assets
                Log.e(TAG, "Native ad finished downloading all assets.");
               /* if (nativeAd == null || nativeAd != ad) {
                    return;
                }
                inflateAd(nativeAd);*/
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                // Native ad failed to load
                Log.e(TAG, "Native ad failed to load: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                // Native ad is loaded and ready to be displayed
                Log.d(TAG, "Native ad is loaded and ready to be displayed!");
                // Race condition, load() called again before last ad was displayed
                if (nativeAd == null || nativeAd != ad) {
                    return;
                }
                // Inflate Native Ad into Container
                inflateAd(nativeAd);
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Native ad clicked
                Log.d(TAG, "Native ad clicked!");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                // Native ad impression
                Log.d(TAG, "Native ad impression logged!");
            }
        });

        // Request an ad
        nativeAd.loadAd();
        showNativeAdWithDelay();
    }

    private void inflateAd(NativeAd nativeAd) {

        nativeAd.unregisterView();

        // Add the Ad view into the ad container.
        nativeAdLayout = findViewById(R.id.nativeAdLayout);
        LayoutInflater inflater = LayoutInflater.from(ConnectActivity.this);
        // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
        LinearLayout adView = (LinearLayout) inflater.inflate(R.layout.custom_layout_fbnative, nativeAdLayout, false);
        nativeAdLayout.addView(adView);

        // Add the AdOptionsView
        LinearLayout adChoicesContainer = findViewById(R.id.ad_choices_container);
        AdOptionsView adOptionsView = new AdOptionsView(ConnectActivity.this, nativeAd, nativeAdLayout);
        adChoicesContainer.removeAllViews();
        adChoicesContainer.addView(adOptionsView, 0);

        // Create native UI using the ad metadata.
        AdIconView nativeAdIcon = adView.findViewById(R.id.native_ad_icon);
        TextView nativeAdTitle = adView.findViewById(R.id.native_ad_title);
        MediaView nativeAdMedia = adView.findViewById(R.id.native_ad_media);
        TextView nativeAdSocialContext = adView.findViewById(R.id.native_ad_social_context);
        TextView nativeAdBody = adView.findViewById(R.id.native_ad_body);
        TextView sponsoredLabel = adView.findViewById(R.id.native_ad_sponsored_label);
        Button nativeAdCallToAction = adView.findViewById(R.id.native_ad_call_to_action);

        // Set the Text.
        nativeAdTitle.setText(nativeAd.getAdvertiserName());
        nativeAdBody.setText(nativeAd.getAdBodyText());
        nativeAdSocialContext.setText(nativeAd.getAdSocialContext());
        nativeAdCallToAction.setVisibility(nativeAd.hasCallToAction() ? View.VISIBLE : View.INVISIBLE);
        nativeAdCallToAction.setText(nativeAd.getAdCallToAction());
        sponsoredLabel.setText(nativeAd.getSponsoredTranslation());

        // Create a list of clickable views
        List<View> clickableViews = new ArrayList<>();
        clickableViews.add(nativeAdTitle);
        clickableViews.add(nativeAdCallToAction);

        // Register the Title and CTA button to listen for clicks.
        nativeAd.registerViewForInteraction(
                adView,
                nativeAdMedia,
                nativeAdIcon,
                clickableViews);
    }

    private void showNativeAdWithDelay() {
        /**
         * Here is an example for displaying the ad with delay;
         * Please do not copy the Handler into your project
         */
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                // Check if nativeAd has been loaded successfully
                if (nativeAd == null || !nativeAd.isAdLoaded()) {
                    return;
                }
                // Check if ad is already expired or invalidated, and do not show ad if that is the case. You will not get paid to show an invalidated ad.
                if (nativeAd.isAdInvalidated()) {
                    return;
                }
                inflateAd(nativeAd); // Inflate Native Ad into Container same as previous code example
            }
        }, 1000 * 30); // Show the ad after 15 minutes
    }

    class GenerateTask extends TimerTask {
        boolean started = false;

        @Override
        public void run() {
            if (started) {
                System.out.println("generating");
                final TextView textGenerateNumber = (TextView) findViewById(R.id.randomCount);
                final int random = ThreadLocalRandom.current().nextInt(1000, 7000 + 1);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        textGenerateNumber.setText(String.valueOf(random + "  Online"));
                    }
                });
            }
        }
    }
}
