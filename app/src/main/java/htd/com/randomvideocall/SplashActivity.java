package htd.com.randomvideocall;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ThreadLocalRandom;

import androidx.appcompat.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {
    AsyncTask<?, ?, ?> newTask = new Async();
    private boolean isLaunched = false;
    boolean firstLaunch;

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        assert cm != null;
        return cm.getActiveNetworkInfo() != null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        final SharedPreferences preference = getSharedPreferences("MyPreference", Context.MODE_PRIVATE);
        firstLaunch = preference.getBoolean("firstlaunch", true);

        RelativeLayout linearLayout = findViewById(R.id.splashBody);
        linearLayout.postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
//                        if (firstLaunch) {
//                            preference.edit().putBoolean("firstlaunch", false).apply();
//                            Intent i = new Intent(SplashActivity.this, IntroActivity.class);
//                            startActivity(i);
//                            finish();
//                        } else {

                            if (isNetworkConnected(SplashActivity.this) && !isLaunched) {
                                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                                isLaunched = true;
                                finish();
                            } else if (!isNetworkConnected(SplashActivity.this)) {
                                Toast.makeText(getApplicationContext(), "Please Check Your Internet Connection", Toast.LENGTH_LONG).show();
                            }
                       // }
                    }
                }, 5000);
        if (newTask != null)
            newTask.cancel(true);
       /* newTask = new Async();
        newTask.execute();*/
        new Async().execute();
    }

    @SuppressLint("StaticFieldLeak")
    private class Async extends AsyncTask<Object, Void, Object> {

        @Override
        protected Void doInBackground(Object... objects) {
            int test = ThreadLocalRandom.current().nextInt(2000, 4000 + 1);
            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            if (isNetworkConnected(SplashActivity.this) && !isLaunched && !firstLaunch) {
                                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                                isLaunched = true;
                                finish();
                            }
                        }
                    });
                }
            };
            new Timer().schedule(task, 0, test);
            return null;
        }
    }
}
