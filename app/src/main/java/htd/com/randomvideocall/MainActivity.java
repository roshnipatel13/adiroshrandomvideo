package htd.com.randomvideocall;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ThreadLocalRandom;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import htd.com.randomvideocall.OneSignalNotification.MyNotificationReceivedHandler;
import htd.com.randomvideocall.OneSignalNotification.NotificationActivity;
import htd.com.randomvideocall.Utils.BadgeDrawable;
import htd.com.randomvideocall.adapter.MainAdapter;
import htd.com.randomvideocall.model.RoomActivityMethods;
import htd.com.randomvideocall.model.RoomModel;
import pub.devrel.easypermissions.EasyPermissions;

import static htd.com.randomvideocall.OneSignalNotification.MyNotificationReceivedHandler.counter;

public class MainActivity extends AppCompatActivity {
    public static LayerDrawable icon;
    AsyncTask<?, ?, ?> newTask = new Async();
    AsyncTask<?, ?, ?> newNotiTask;
    RecyclerView mRecycler;
    MainAdapter adapter;
    private Toolbar toolbar;
    ArrayList<RoomModel> roomModelArrayList;
    String[] PERMISSIONS = {"android.permission.CAMERA", "android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE"};
    MenuItem itemCart;
    private TextView toolbar_title;

    public static void setBadgeCount(Context context, LayerDrawable icon, String count) {
        BadgeDrawable badge;        // Reuse drawable if possible
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_badge);
        if (reuse != null && reuse instanceof BadgeDrawable) {
            badge = (BadgeDrawable) reuse;
        } else {
            badge = new BadgeDrawable(context);
        }
        badge.setCount(count);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_badge, badge);
    }

    void setRecycler() {
        adapter.notifyDataSetChanged();
        mRecycler.invalidate();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences prefs1 = getSharedPreferences("RVdoNotificationCounter", MODE_PRIVATE);
        counter = prefs1.getInt("notificationCounterRVdo", counter);

        invalidateOptionsMenu();

        this.toolbar = findViewById(R.id.toolbar);
        this.toolbar_title = findViewById(R.id.toolbar_title);
        setSupportActionBar(this.toolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle("\t Random Video");
        toolbar.inflateMenu(R.menu.notification_menu);

        mRecycler = findViewById(R.id.mainRecycler);
        LinearLayout adviewBanner = findViewById(R.id.fbbanner);
        if (CentralFbAds.isNetworkConnected(this)) {
            adviewBanner.addView(CentralFbAds.SetupBannerAds(0));
        }

        EasyPermissions.requestPermissions(this, "For the best Random Video Call experience, please Allow Permission", 123, PERMISSIONS);

        if (newTask != null)
            newTask.cancel(true);

        /*newTask = new Async();
        newTask.execute();*/

         new Async().execute();

        roomModelArrayList = new RoomActivityMethods().GetRoomDetails(this);

        adapter = new MainAdapter(this, roomModelArrayList);
        mRecycler.setLayoutManager(new GridLayoutManager(this, 1));
        mRecycler.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.notification_menu, menu);

        itemCart = menu.findItem(R.id.action_notification);
        icon = (LayerDrawable) itemCart.getIcon();

        if (counter > 99) {
            setBadgeCount(this, icon, "99+");
        } else {
            setBadgeCount(this, icon, "" + counter);
        }

        if (newNotiTask != null)
            newNotiTask.cancel(true);
       /* newNotiTask = new NotificationAsync();
        newNotiTask.execute();*/
        new NotificationAsync().execute();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.action_notification) {
            startActivity(new Intent(getApplicationContext(), NotificationActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences.Editor editor1 = getSharedPreferences("RVdoNotificationCounter", MODE_PRIVATE).edit();
        editor1.putInt("notificationCounterRVdo", MyNotificationReceivedHandler.counter);
        editor1.apply();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        SharedPreferences.Editor editor1 = getSharedPreferences("RVdoNotificationCounter", MODE_PRIVATE).edit();
        editor1.putInt("notificationCounterRVdo", MyNotificationReceivedHandler.counter);
        editor1.apply();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        SharedPreferences.Editor editor1 = getSharedPreferences("RVdoNotificationCounter", MODE_PRIVATE).edit();
        editor1.putInt("notificationCounterRVdo", MyNotificationReceivedHandler.counter);
        editor1.apply();

        if (newTask != null)
            newTask.cancel(true);

        if (newNotiTask != null)
            newNotiTask.cancel(true);

        Intent exit = new Intent(MainActivity.this,ExitActivity.class);
        startActivity(exit);
    }

    @SuppressLint("StaticFieldLeak")
    private class Async extends AsyncTask<Object, Void, Object> {

        @Override
        protected Void doInBackground(Object... objects) {
            invalidateOptionsMenu();
            int test = ThreadLocalRandom.current().nextInt(2000, 4000 + 1);
            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setRecycler();
                        }
                    });
                }
            };
            new Timer().schedule(task, 1000, test);
            return null;
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class NotificationAsync extends AsyncTask<Object, Void, Object> {

        @Override
        protected Void doInBackground(Object... objects) {
            invalidateOptionsMenu();
            return null;
        }
    }
}