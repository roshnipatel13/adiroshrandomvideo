package htd.com.randomvideocall;

import android.os.Bundle;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

public class AboutusActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aboutus);

        LinearLayout adviewBanner = findViewById(R.id.fbbanner);
        if (CentralFbAds.isNetworkConnected(this)) {
            adviewBanner.addView(CentralFbAds.SetupBannerAds(0));
        }
    }
}
