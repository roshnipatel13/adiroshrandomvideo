package htd.com.randomvideocall;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.facebook.ads.AudienceNetworkAds;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.InterstitialAdListener;
import com.onesignal.OneSignal;

import java.io.File;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;
import htd.com.randomvideocall.OneSignalNotification.MyNotificationOpenedHandler;
import htd.com.randomvideocall.OneSignalNotification.MyNotificationReceivedHandler;

public class CentralFbAds extends MultiDexApplication {

    public static InterstitialAd interstitialAd;
    public static CentralFbAds cApp;
    public static AdView adView;
    private static String cAppDirectory;
    private static SharedPreferences sharedPreferences;
    int a;


    private static Context context;

    public static Context getContext() {
        return context;
    }

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        assert cm != null;
        return cm.getActiveNetworkInfo() != null;
    }

    public static AdView SetupBannerAds(int size) {
        if (size == 0) {
            adView = new AdView(cApp, "625433724951291_625435931617737", AdSize.BANNER_HEIGHT_50);
        } else if (size == 1) {
            adView = new AdView(cApp, "625433724951291_625435931617737", AdSize.BANNER_HEIGHT_90);
        }
        adView.setAdListener(new AdListener() {
            @Override
            public void onError(Ad ad, AdError adError) {
                // Ad error callback
                Log.e("Can't load Banner Ads", adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                // Ad loaded callback

                // Logger.Print(">>> Banner loaded >> ");
                Log.i("bbbb", ">>> Banner loaded >> ");
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Ad clicked callback
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                // Ad impression logged callback
            }
        });
        // Request an ad
        adView.loadAd();
        return adView;
    }

    @NonNull
    public static CentralFbAds getInstance(@NonNull Context context) {
        return cApp == null ? (CentralFbAds) context.getApplicationContext() : cApp;
    }

    @Nullable
    public static CentralFbAds getInstanceUnsafe() {
        return cApp;
    }

    public static int getSorting() {
        return sharedPreferences.getInt("sorting", 3);
    }

    public static void setSorting(int name) {
        sharedPreferences.edit().putInt("sorting", name).apply();
    }

    @NonNull
    public static String getAppDirectory() {
        if (cAppDirectory == null) {
            cAppDirectory = Environment.getExternalStorageDirectory() + File.separator + "videos_lzl";
        }
        return cAppDirectory;
    }



    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        cApp = this;
        AudienceNetworkAds.initialize(this);
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .setNotificationOpenedHandler(new MyNotificationOpenedHandler())
                .setNotificationReceivedHandler( new MyNotificationReceivedHandler())
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static void SetupInterstitialAds() {

        interstitialAd = new InterstitialAd(cApp, "625433724951291_625434571617873");

        interstitialAd.setAdListener(new InterstitialAdListener() {
            @Override
            public void onInterstitialDisplayed(Ad ad) {
                // Interstitial ad displayed callback
                // Logger.Print("Interstitial ad displayed.");
                Log.i("cccc", "Interstitial ad displayed.");
            }

            @Override
            public void onInterstitialDismissed(Ad ad) {
                // Interstitial dismissed callback
                // Logger.Print("Interstitial ad dismissed.");
                Log.i("dddd", "Interstitial ad dismissed.");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        interstitialAd.loadAd();
                    }
                }, 60000);
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                // Ad error callback
                //Logger.Print("Interstitial ad failed to load: " + adError.getErrorMessage());
                Log.i("eeee", "Interstitial ad failed to load:");
            }

            @Override
            public void onAdLoaded(Ad ad) {
                // Interstitial ad is loaded and ready to be displayed
                //Logger.Print("Interstitial ad is loaded and ready to be displayed!");
                // Show the ad
                Log.i("ffff", "Interstitial ad is loaded and ready to be displayed!");
                interstitialAd.show();
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Ad clicked callback
                //Logger.Print("Interstitial ad clicked!");
                Log.i("gggg", "Interstitial ad clicked!");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                // Ad impression logged callback
                //Logger.Print("Interstitial ad impression logged!");
                Log.i("hhhh", "Interstitial ad impression logged!");
            }
        });

        // For auto play video ads, it's recommended to load the ad
        // at least 30 seconds before it is shown
        interstitialAd.loadAd();

    }

    public static void ShowAds() {
        if (interstitialAd != null && interstitialAd.isAdLoaded()) {
            interstitialAd.show();
        }
    }


}
