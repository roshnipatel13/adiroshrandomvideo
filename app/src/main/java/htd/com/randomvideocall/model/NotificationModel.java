package htd.com.randomvideocall.model;

public class NotificationModel {
    public String notification_Text;
    public String notification_Img;
    public String notification_launchUrl;
    public int notification_Id;

    public int getNotification_Id() {
        return notification_Id;
    }

    public void setNotification_Id(int notification_Id) {
        this.notification_Id = notification_Id;
    }

    public String getNotification_Img() {
        return notification_Img;
    }

    public String getNotification_launchUrl() {
        return notification_launchUrl;
    }

    public String getNotification_Text() {
        return notification_Text;
    }

    public void setNotification_Img(String notification_Img) {
        this.notification_Img = notification_Img;
    }

    public void setNotification_Text(String notification_Text) {
        this.notification_Text = notification_Text;
    }

    public void setNotification_launchUrl(String notification_launchUrl) {
        this.notification_launchUrl = notification_launchUrl;
    }
}
