package htd.com.randomvideocall.model;

import android.content.Context;

import java.util.ArrayList;

import htd.com.randomvideocall.R;

public class RoomActivityMethods {
    public ArrayList<RoomModel> GetRoomDetails(Context context) {
        ArrayList<RoomModel> arrayList = new ArrayList<>();

        RoomModel roomModel1 = new RoomModel();
        roomModel1.setRoomImg(R.drawable.room1);
        roomModel1.setRoomNo(context.getResources().getString(R.string.room1));
        roomModel1.setRoomType(context.getResources().getString(R.string.IndiaDesi));
        roomModel1.setRoomCount(context.getResources().getString(R.string.Count));
        arrayList.add(roomModel1);

        RoomModel roomModel2 = new RoomModel();
        roomModel2.setRoomImg(R.drawable.room2);
        roomModel2.setRoomNo(context.getResources().getString(R.string.room2));
        roomModel2.setRoomType(context.getResources().getString(R.string.IndiaStrange));
        roomModel2.setRoomCount(context.getResources().getString(R.string.Count));
        arrayList.add(roomModel2);

        RoomModel roomModel3 = new RoomModel();
        roomModel3.setRoomImg(R.drawable.room3);
        roomModel3.setRoomNo(context.getResources().getString(R.string.room3));
        roomModel3.setRoomType(context.getResources().getString(R.string.UsaModels));
        roomModel3.setRoomCount(context.getResources().getString(R.string.Count));
        arrayList.add(roomModel3);

        RoomModel roomModel4 = new RoomModel();
        roomModel4.setRoomImg(R.drawable.room4);
        roomModel4.setRoomNo(context.getResources().getString(R.string.room4));
        roomModel4.setRoomType(context.getResources().getString(R.string.Usagirls));
        roomModel4.setRoomCount(context.getResources().getString(R.string.Count));
        arrayList.add(roomModel4);

        RoomModel roomModel5 = new RoomModel();
        roomModel5.setRoomImg(R.drawable.room5);
        roomModel5.setRoomNo(context.getResources().getString(R.string.room5));
        roomModel5.setRoomType(context.getResources().getString(R.string.UsaStrange));
        roomModel5.setRoomCount(context.getResources().getString(R.string.Count));
        arrayList.add(roomModel5);

        RoomModel roomModel6 = new RoomModel();
        roomModel6.setRoomImg(R.drawable.room6);
        roomModel6.setRoomNo(context.getResources().getString(R.string.room6));
        roomModel6.setRoomType(context.getResources().getString(R.string.UsaDating));
        roomModel6.setRoomCount(context.getResources().getString(R.string.Count));
        arrayList.add(roomModel6);

        RoomModel roomModel7 = new RoomModel();
        roomModel7.setRoomImg(R.drawable.room7);
        roomModel7.setRoomNo(context.getResources().getString(R.string.room7));
        roomModel7.setRoomType(context.getResources().getString(R.string.BangkokAdult));
        roomModel7.setRoomCount(context.getResources().getString(R.string.Count));
        arrayList.add(roomModel7);

        RoomModel roomModel8 = new RoomModel();
        roomModel8.setRoomImg(R.drawable.room8);
        roomModel8.setRoomNo(context.getResources().getString(R.string.room8));
        roomModel8.setRoomType(context.getResources().getString(R.string.ThailandGirls));
        roomModel8.setRoomCount(context.getResources().getString(R.string.Count));
        arrayList.add(roomModel8);

        RoomModel roomModel9 = new RoomModel();
        roomModel9.setRoomImg(R.drawable.room9);
        roomModel9.setRoomNo(context.getResources().getString(R.string.room9));
        roomModel9.setRoomType(context.getResources().getString(R.string.BangkokHot));
        roomModel9.setRoomCount(context.getResources().getString(R.string.Count));
        arrayList.add(roomModel9);

        RoomModel roomModel10 = new RoomModel();
        roomModel10.setRoomImg(R.drawable.room10);
        roomModel10.setRoomNo(context.getResources().getString(R.string.room10));
        roomModel10.setRoomType(context.getResources().getString(R.string.CanadaStrange));
        roomModel10.setRoomCount(context.getResources().getString(R.string.Count));
        arrayList.add(roomModel10);

        RoomModel roomModel11 = new RoomModel();
        roomModel11.setRoomImg(R.drawable.room11);
        roomModel11.setRoomNo(context.getResources().getString(R.string.room11));
        roomModel11.setRoomType(context.getResources().getString(R.string.CanadaHot));
        roomModel11.setRoomCount(context.getResources().getString(R.string.Count));
        arrayList.add(roomModel11);

        RoomModel roomModel12 = new RoomModel();
        roomModel12.setRoomImg(R.drawable.room12);
        roomModel12.setRoomNo(context.getResources().getString(R.string.room12));
        roomModel12.setRoomType(context.getResources().getString(R.string.IndiaDesiGirl));
        roomModel12.setRoomCount(context.getResources().getString(R.string.Count));
        arrayList.add(roomModel12);

        return arrayList;
    }
}
