package htd.com.randomvideocall.model;

public class RoomModel {
    public String RoomNo, RoomType, RoomCount;
    public int RoomImg;

    public int getRoomImg() {
        return RoomImg;
    }

    public String getRoomCount() {
        return RoomCount;
    }

    public String getRoomNo() {
        return RoomNo;
    }

    public String getRoomType() {
        return RoomType;
    }

    public void setRoomCount(String roomCount) {
        RoomCount = roomCount;
    }

    public void setRoomImg(int roomImg) {
        RoomImg = roomImg;
    }

    public void setRoomNo(String roomNo) {
        RoomNo = roomNo;
    }

    public void setRoomType(String roomType) {
        RoomType = roomType;
    }
}

