package htd.com.randomvideocall.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import htd.com.randomvideocall.CentralFbAds;
import htd.com.randomvideocall.DbHelper.OneSignalDBHelper;
import htd.com.randomvideocall.OneSignalNotification.ItemTouchHelperClass;
import htd.com.randomvideocall.OneSignalNotification.MyNotificationReceivedHandler;
import htd.com.randomvideocall.R;
import htd.com.randomvideocall.Utils.UIUtils;
import htd.com.randomvideocall.model.NotificationModel;


public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder> implements ItemTouchHelperClass.ItemTouchHelperAdapter {
    public Context mContext;
    public OneSignalDBHelper mydb = new OneSignalDBHelper(CentralFbAds.getContext());
    private ArrayList<NotificationModel> notificationArrayList;

    public NotificationAdapter(Context context, ArrayList<NotificationModel> notificationModelList) {
        this.mContext = context;
        notificationArrayList = notificationModelList;
    }

    @NonNull
    @Override
    public NotificationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_notification_layout, parent, false);
        return new NotificationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationViewHolder holder, final int position) {

        if (notificationArrayList.get(position) != null) {

            if (notificationArrayList.get(position).getNotification_Text() != null) {
                holder.notificationText.setText(notificationArrayList.get(position).getNotification_Text());
            } else {
                holder.notificationText.setText("Notification Title");
            }

            if (notificationArrayList.get(position).getNotification_launchUrl() != null) {
                holder.notificationLink.setText(notificationArrayList.get(position).getNotification_launchUrl());
            } else {
                holder.notificationLink.setText("LaunchURL");
            }
            Glide.with(mContext)
                    .asBitmap()
                    .load(notificationArrayList.get(position).getNotification_Img())
                    .fitCenter()
                    .into(UIUtils.getRoundedImageTarget(mContext, holder.notificationImg, 0));

        }

        holder.relativeLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int Notification_ID = notificationArrayList.get(position).getNotification_Id();

                if (notificationArrayList.get(position).getNotification_launchUrl() != null) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("" + notificationArrayList.get(position).getNotification_launchUrl()));
                    mContext.startActivity(browserIntent);
                    MyNotificationReceivedHandler.counter = 0;
                    mydb.deleteNotification(Notification_ID);
                    ((Activity) mContext).finish();

                } else {

                    MyNotificationReceivedHandler.counter = 0;
                    mydb.deleteNotification(Notification_ID);
                    ((Activity) mContext).finish();
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return notificationArrayList != null ? notificationArrayList.size() : 0;
    }

    @Override
    public void onItemMoved(int fromPosition, int toPosition) {

    }

    @Override
    public void onItemRemoved(int position) {

        try {
            int Notification_ID = notificationArrayList.get(position).getNotification_Id();
            mydb.deleteNotification(Notification_ID);
            if (notificationArrayList.get(position) != null)
                notificationArrayList.remove(position);
        } catch (Exception e) {
            e.printStackTrace();
        }

        notifyItemRemoved(position);
    }

    public static class NotificationViewHolder extends RecyclerView.ViewHolder {
        public TextView notificationLink, notificationText;
        public ImageView notificationImg;
        public RelativeLayout relativeLay;

        public NotificationViewHolder(@NonNull View itemView) {
            super(itemView);
            notificationImg = itemView.findViewById(R.id.notificationImg);
            notificationText = itemView.findViewById(R.id.notificationText);
            notificationLink = itemView.findViewById(R.id.notificationLink);
            relativeLay = itemView.findViewById(R.id.relativeLay);

        }

    }
}