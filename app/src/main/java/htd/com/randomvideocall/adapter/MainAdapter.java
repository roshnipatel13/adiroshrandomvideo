package htd.com.randomvideocall.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.ads.InterstitialAd;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import htd.com.randomvideocall.CentralFbAds;
import htd.com.randomvideocall.ConnectActivity;
import htd.com.randomvideocall.R;
import htd.com.randomvideocall.model.RoomModel;

public class MainAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public List<RoomModel> mRoom;
    private Context mContext;
    private LayoutInflater mInflater;
    public MainAdapter(Context context, List<RoomModel> roomModels) {
        this.mInflater = LayoutInflater.from(context);
        this.mRoom = roomModels;
        this.mContext = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case 0:
                //google native ad code
//                RecyclerView.ViewHolder viewHolder1;
//                LayoutInflater inflater1 = LayoutInflater.from(parent.getContext());
//                View v = inflater1.inflate(R.layout.item_layout_ad, parent, false);
//                viewHolder1 = new AdViewHolder(v);
//                return viewHolder1;

            default:
                RecyclerView.ViewHolder viewHolder;
                LayoutInflater inflater = LayoutInflater.from(parent.getContext());
                View v1 = inflater.inflate(R.layout.main_single_item_view, parent, false);
                viewHolder = new LayoutHolder(v1);
                return viewHolder;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 0:
                //google native ad code
//                final AdViewHolder holder2 = (AdViewHolder) holder;
//                styles = new NativeTemplateStyle.Builder().build();
//                MobileAds.initialize(context, context.getString(R.string.nativead));
//                AdLoader adLoader = new AdLoader.Builder(context, context.getString(R.string.nativead))
//                        .forUnifiedNativeAd(new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
//                            @Override
//                            public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
//
//                                holder2.template.setStyles(styles);
//                                holder2.template.setNativeAd(unifiedNativeAd);
//
//                            }
//                        })
//                        .build();
//
//                adLoader.loadAd(new AdRequest.Builder().build());

                break;
            default:
                int test = ThreadLocalRandom.current().nextInt(1000, 7000 + 1);
                RoomModel cardItem = mRoom.get(position);
                LayoutHolder holder1 = (LayoutHolder) holder;
                holder1.roomNo.setText(cardItem.getRoomNo());
                holder1.roomType.setText(cardItem.getRoomType());
                holder1.roomCount.setText(test + " Online");
                holder1.roomImg.setImageResource(cardItem.getRoomImg());


                holder1.rl_feature_item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent myActivity = new Intent(mContext.getApplicationContext(), ConnectActivity.class);
                        myActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mContext.getApplicationContext().startActivity(myActivity);
                    }
                });
        }

    }

    @Override
    public int getItemViewType(int position) {

//        if (position % 3 == 2) {
//            return ADS;
//        } else return 1;
        return 1;
    }

    @Override
    public int getItemCount() {
        return mRoom.size();
    }

    private class LayoutHolder extends RecyclerView.ViewHolder {
        LinearLayout rl_feature_item;
        ImageView roomImg;
        TextView roomNo, roomType, roomCount;

        public LayoutHolder(View itemView) {
            super(itemView);
            rl_feature_item = itemView.findViewById(R.id.rl_feature_item);
            roomImg = itemView.findViewById(R.id.roomImg);
            roomNo = itemView.findViewById(R.id.roomNo);
            roomType = itemView.findViewById(R.id.roomType);
            roomCount = itemView.findViewById(R.id.roomCount);
        }
    }
}
