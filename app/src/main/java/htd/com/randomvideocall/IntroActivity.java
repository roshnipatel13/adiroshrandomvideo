package htd.com.randomvideocall;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.model.SliderPage;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import htd.com.randomvideocall.fragments.AppIntroCustomFragment;

public class IntroActivity extends AppIntro {
    com.github.paolorotolo.appintro.AppIntroViewPager viewPager;

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addSlide(AppIntroCustomFragment.newInstance(createSlidepage("", "", R.drawable.ic_launcher_background, Color.parseColor("#2a415e"))));
        addSlide(AppIntroCustomFragment.newInstance(createSlidepage("", "", R.drawable.ic_launcher_background, Color.parseColor("#2a415e"))));
        addSlide(AppIntroCustomFragment.newInstance(createSlidepage("", "", R.drawable.ic_launcher_background, Color.parseColor("#2a415e"))));
        addSlide(AppIntroCustomFragment.newInstance(createSlidepage("", "", R.drawable.ic_launcher_background, Color.parseColor("#2a415e"))));

        // OPTIONAL METHODS
        // Override bar/separator color.
        setBarColor(Color.parseColor("#00000000"));
        setSeparatorColor(R.color.colorPrimaryLight);


        // Hide Skip/Done button.
        showSkipButton(true);

        // Turn vibration on and set intensity.
        // NOTE: you will probably need to ask VIBRATE permission in Manifest.
        setVibrate(true);
        setVibrateIntensity(30);
    }

    public SliderPage createSlidepage(String title, String desc, int Image, int color) {
        SliderPage sliderPage = new SliderPage();
        sliderPage.setTitle(title);
        sliderPage.setDescription(desc);
        sliderPage.setImageDrawable(Image);
        sliderPage.setBgColor(color);

        return sliderPage;
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        // Do something when users tap on Skip button.
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
        finish();
    }


    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        // Do something when users tap on Done button.
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);
        // Do something when the slide changes.
    }
}
